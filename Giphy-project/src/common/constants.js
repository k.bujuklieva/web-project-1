export const HOME = 'home';

export const TRENDING = 'trending';

export const UPLOAD = 'upload';

export const MY_UPLOADS = 'my-uploads';

export const FAVORITES = 'favorites';

export const ABOUT = 'about';

export const CONTAINER_SELECTOR = '#container';

export const FULL_HEART = '❤';

export const EMPTY_HEART = '♡';

export const SHOW_DETAILS = 'View details';

export const HIDE_DETAILS = 'Hide details';

export const API_KEY = 'HzWe5iNiyJx1wqYRpRyX3TJ9umSaV3Xv';

export const SEARCH_LIMIT = 15;

export const TRENDING_LIMIT = 15;