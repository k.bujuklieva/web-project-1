import { EMPTY_HEART, FULL_HEART } from '../common/constants.js';
import { getFavorites } from '../data/favorites.js';

/**
 * easy to use shorthand for selection of element 
 * @param {string} selector 
 * @returns the selected element
 */
export const q = (selector) => document.querySelector(selector);

/**
 * easy to use shorthand for selection of all elements
 * @param {string} selector 
 * @returns the selected elements
 */
export const qs = (selector) => document.querySelectorAll(selector);

/**
 * sets status active of navigation elements
 * @param {string} page 
 */
export const setActiveNav = (page) => {
    const navs = qs('a.nav-link');

    Array
        .from(navs)
        .forEach(element => element
            .getAttribute('data-page') === page ?
            element.classList.add('active') :
            element.classList.remove('active')
        );
};

/**
 * renders template string for favorite status
 * @param {string} gifId 
 * @returns template string with either full or empty heart
 */
export const renderFavoriteStatus = (gifId) => {
    const favorites = getFavorites();

    return favorites.includes(gifId) ?
        `<span class="favorite active" data-gif-id="${gifId}">${FULL_HEART}</span>` :
        `<span class="favorite" data-gif-id="${gifId}">${EMPTY_HEART}</span>`;
};