import { loadSingleGifDetails } from '../requests/request-service.js';
import { HIDE_DETAILS, SHOW_DETAILS } from '../common/constants.js';
import { q } from './helpers.js';

/**
 * toggles show/hide details: changes overlay with gif details view
 * @param {string} gifId 
 */
export const toggleDetails = async(gifId) => {

    const gif = await loadSingleGifDetails(gifId);

    if (q(`button[data-gif-id="${gifId}"]`).innerHTML === `${SHOW_DETAILS}`) {
        q(`.overlay[data-gif-id="${gif.gifDetails[0]}"`).style.opacity = 1;
        q(`.text[data-gif-id="${gif.gifDetails[0]}"`).innerHTML = `
        <ul class="details-list">
            <li>
                <b>Title:</b> ${gif.gifDetails[1] || "N/A"}
            </li>
            <li>
                <b>Uploaded by:</b> ${gif.gifDetails[2]|| "N/A"}
            </li>
            <li>
                <b>Short url:</b> ${gif.gifDetails[3]|| "N/A"}
            </li>
            <li>
                <b>Source url:</b> ${gif.gifDetails[4]|| "N/A"}
            </li>
        </ul>
    `;
        q(`button[data-gif-id="${gif.gifDetails[0]}"]`).innerHTML = `${HIDE_DETAILS}`
    } else {
        q(`.overlay[data-gif-id="${gif.gifDetails[0]}"`).style.opacity = 0;
        q(`button[data-gif-id="${gif.gifDetails[0]}"]`).innerHTML = `${SHOW_DETAILS}`;
    }
};