import { q } from './helpers.js';
import { API_KEY } from '../common/constants.js';

/**
 * async function that processes the gif upload fetch POST request
 *  after validations for content type and 'no-file';
 * @param {event} event 
 * @returns Promise - response of the async request to upload gif file
 */
export const processForm = async(event) => {
    event.preventDefault();

    const formElement = q("#uploads-form");
    const file = q("#myFile").files[0];

    if (!file) {
        throw new Error('No file attached!');
    }
    const formData = new FormData(formElement);

    return await fetch(`http://upload.giphy.com/v1/gifs?api_key=${API_KEY}`, {
            method: 'POST',
            body: formData
        })
        .then(res => res.json())
        .then(json => {
            return { gifId: json.data.id, status: json.meta.status }
        });
};