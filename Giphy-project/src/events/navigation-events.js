import { CONTAINER_SELECTOR, HOME, TRENDING, UPLOAD, FAVORITES, MY_UPLOADS, ABOUT } from '../common/constants.js';
import { toHomeView } from '../views/home-view.js';
import { q, setActiveNav } from './helpers.js';
import { getFavorites } from './../data/favorites.js';
import { getUploads } from '../data/uploads.js';
import { toFavoritesView } from '../views/favorites-view.js';
import { toAboutView } from '../views/about-view.js';
import { toTrendingView } from '../views/trending-view.js';
import { toMyUploadsView } from '../views/uploads-view.js';
import { toEmptyUploadsView } from '../views/empty-uploads-view.js ';
import { toRandomView } from '../views/random-view.js';
import { toUploadFormView } from '../views/upload-form-view.js';
import { loadTrendingGifs, loadRandomGif, loadFavoriteGifs, loadUploadedGifs } from '../requests/request-service.js'

/**
 * 
 * @param {string} page name of the page to be loaded
 * @returns renderHome() which processes the various views
 */
export const loadPage = (page = '') => {

    switch (page) {

        case HOME:
            setActiveNav(HOME);
            return renderHome();
        case TRENDING:
            setActiveNav(TRENDING);
            return renderTrending();
        case UPLOAD:
            setActiveNav(UPLOAD);
            return renderUpload();
        case FAVORITES:
            setActiveNav(FAVORITES);
            return renderFavorites();
        case MY_UPLOADS:
            setActiveNav(MY_UPLOADS);
            return renderMyUploads();
        case ABOUT:
            setActiveNav(ABOUT);
            return renderAbout();
        default:
            return null;
    }
};

/**
 * async function that renders Home view
 */
const renderHome = async() => {

    const gifs = await loadTrendingGifs();
    q(CONTAINER_SELECTOR).innerHTML = await toHomeView(gifs);
};

/**
 * async function that renders Trending view
 */
const renderTrending = async() => {

    const gifs = await loadTrendingGifs();
    q(CONTAINER_SELECTOR).innerHTML = toTrendingView(gifs);
};

/**
 * async function that Upload Form view
 */
const renderUpload = async() => {
    // add loading;
    q(CONTAINER_SELECTOR).innerHTML = toUploadFormView();
};

/**
 * async function that renders Favorites view or Random view depending on count of favorite gifs
 */
const renderFavorites = async() => {
    const favorites = getFavorites();

    if (favorites.length > 0) {
        const gifIds = loadFavoriteGifs();

        q(CONTAINER_SELECTOR).innerHTML = await toFavoritesView(gifIds);
    } else {
        await renderRandom();
    }
};

/**
 * async function that renders My Uploads view
 */
const renderMyUploads = async() => {
    const uploads = getUploads();

    if (uploads.length > 0) {
        const gifs = await loadUploadedGifs();

        q(CONTAINER_SELECTOR).innerHTML = toMyUploadsView(gifs);
    } else {
        q(CONTAINER_SELECTOR).innerHTML = toEmptyUploadsView();

    }
};

/**
 * async function that renders About view
 */
const renderAbout = () => {
    q(CONTAINER_SELECTOR).innerHTML = toAboutView();
};

/**
 * async function that renders Random gif view
 */
const renderRandom = async() => {
    const random = await loadRandomGif();

    q(CONTAINER_SELECTOR).innerHTML = toRandomView(random);
};