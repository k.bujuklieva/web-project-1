import { toSearchView } from '../views/search-view.js';
import { q } from './helpers.js';
import { CONTAINER_SELECTOR } from './../common/constants.js';
import { loadSearchGifs } from '../requests/request-service.js'

/**
 * async function that renders Searched gifs by searchTerm view
 * @param {string} searchTerm 
 */
export const renderSearchItems = async(searchTerm) => {
    const matchedGifs = await loadSearchGifs(searchTerm);

    q(CONTAINER_SELECTOR).innerHTML = toSearchView(matchedGifs, searchTerm);
};