import { toTrendingView } from "./trending-view.js";

/**
 * template string of Home page container view
 */
export const toHomeView = async(gifs) => `
  <div id="home">
    <h1>Welcome to .gif bash</h1>
    <div id="home-info">
      <p>Simple gif manager that uses the GIPHY API. You can:</p>
      <ul>
        <li>Browse any gifs by search term</li>
        <li>Check out latest trending gifs</li>
        <li>Upload your own gifs and view them</li>
        <li>Add and remove gifs from favorites</li>
        <li>Get more details on gifs</li>
    </ul>
    </div>
    <div id="trending-home">
    ${toTrendingView(gifs)}
    </div>
    </div>
  </div>`;