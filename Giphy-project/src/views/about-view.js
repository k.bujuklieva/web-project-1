/**
 * template string of About page container view
 */
export const toAboutView = () => `
<div id="about">
  <h1>About .gif bash</h1>
    <h2> This <a href="https://giphy.com" id="giphy-url"> GIPHY </a> manager was made as part of the Web Development Module in Telerik Academy (A32JS). 
    <h3>We are using the GIPHY API ENDPOINTS to demonstrate various common functionalities on web pages such as:</h3>
    <ul id="features">
      <li>  displaying limited search results dynamically- using the Search API Endpoint</li>
      <li>  displaying limited trending gifs- using the Trending API Endpoint</li>
      <li>  like/unlike toggle for gifs</li>
      <li>  displaying favorite gifs (saved in localStorage) - using the GetGifById API Endpoint</li>
      <li>  displaying a random gif (if no favorites added yet )- using the Random API Endpoint</li>
      <li>  uploading gifs and displaying uploaded gifs (saved in localStorage) - using the GetGifsById API Endpoint</li>
    </ul>
  </div>
</div>
`;