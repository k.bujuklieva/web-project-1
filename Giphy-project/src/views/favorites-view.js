import { gifSimpleView } from "./gif-simple-view.js";
import { getGifById } from "../data/gifs.js";

/**
 * template string of Favorites page container view
 */
export const toFavoritesView = async(gifIds) => `
<div id="favorites-view gif-view">
  <h1> My Favorite gifs: </h1>
    <div class="favorites-container content">
      ${(await Promise.all(gifIds.map(async (id) => getGifById(id)))).map(obj => gifSimpleView(obj.gifInfo))}
    </div>
</div>
`;