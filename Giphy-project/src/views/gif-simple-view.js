import { renderFavoriteStatus, q } from "../events/helpers.js";
import { SHOW_DETAILS } from "../common/constants.js";
/**
 * 
 * @param {obj} gifInfo 
 * @returns template string of a simple single view card-box of a gif
 */
export const gifSimpleView = (gifInfo) => `
  <div class="card-box">
    <img src=${gifInfo[0]} data-gif-id = "${gifInfo[1]}" class = "gif-img">
    <div class="overlay" data-gif-id="${gifInfo[1]}">
      <div class="text" data-gif-id="${gifInfo[1]}">

      </div>
    </div>
    <div id="card-data">
      <button class="details-label" data-gif-id="${gifInfo[1]}">${SHOW_DETAILS}</button>
      ${renderFavoriteStatus(gifInfo[1])}
    </div>
  </div>`;