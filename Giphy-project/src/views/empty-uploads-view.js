/**
 * template string of My Uploads page container view if no uploads found
 */
export const toEmptyUploadsView = () => `
  <div class="no-uploads">
    <h1> Oh no!You haven't uploaded anything yet. :( </h1>  
    <h3> Click on the UPLOAD button on the navigation to upload your first .gif now! </h3> 
  </div>`;