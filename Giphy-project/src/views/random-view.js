import { gifSimpleView } from "./gif-simple-view.js";
/**
 * template string of Random container view (if no favorites found)
 */
export const toRandomView = (random) => `
  <div id="random-view"">
    <div id="random-label" >
      <h1> Oh, no! You don't have any favorite gifs yet. </h1> 
      <h3> Here is a random gif we have chosen for you :) </h3> 
    </div> 
    <div id="random-gif">
      ${gifSimpleView(random)} 
    </div>
  </div>
`;