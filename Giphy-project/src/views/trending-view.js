import { gifSimpleView } from "./gif-simple-view.js";

/**
 * template string of Trending page container view
 * @param {{obj}} gifs
 */
export const toTrendingView = (gifs) => `
<div id="trending-view" class="gif-view">
  <h1>Trending gifs</h1>
  <div class="content">
    ${gifs.gifInfo.map(gifSimpleView).join('\n')}
  </div>
</div>
`;