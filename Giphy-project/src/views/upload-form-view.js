/**
 * template string of Upload Form page container view
 */
export const toUploadFormView = () => `
<div>
  <h1>Upload your .gif here! </h1>
  <form id="uploads-form" method="post">
      <div>
        <label for="file"> File</label>
        <input type="file" id="myFile" name="file" accept="image/gif">
        <p id="file-validation"> Oops, you forgot to attach a file! Please, upload a valid file!</p>
        <p id="uploads-validation"> Acceptable formats: .gif </p>
        <button type="submit" id="upload-button"> Upload now! </button>
        <p id="upload-success"> YAY! Your gif has been uploaded successfully! </p>
      </div>
    </form>
</div>
`;