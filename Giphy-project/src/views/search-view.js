import { gifSimpleView } from './gif-simple-view.js';

/**
 * template string of Search page container view
 * @param {{obj}} matchedGifs
 * @param {string} searchTerm
 */
export const toSearchView = (matchedGifs, searchTerm) => `
  <div id="search-results" class="gif-view">
    <h1>Showing ${matchedGifs.showing} gifs out of ${matchedGifs.total} matches for "${searchTerm}":</h1>
    <div class="content">
    ${matchedGifs.gifInfo.map(gifSimpleView).join('\n')}
    </div>
  </div>
`;