import { gifSimpleView } from "./gif-simple-view.js";

/**
 * template string of My Uploads page container view
 * @param {{obj}} gifs
 */
export const toMyUploadsView = (gifs) => `
  <div id="uploads-view">
    <div id="uploads-label">
      <h1> My Uploads </h1>
    </div>
    <div id="uploaded-gifs">
      ${gifs.gifInfo.map(gifSimpleView).join('\n')}
    </div>
    <br>
  </div>
`;