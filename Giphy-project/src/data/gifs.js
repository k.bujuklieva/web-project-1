import { API_KEY, SEARCH_LIMIT, TRENDING_LIMIT } from '../common/constants.js'

/**
 * async fetch GET request to get search results on search terms
 * @param {string} searchTerm 
 * @returns Promise with obj with matching gifs
 */
export const searchGifs = async(searchTerm = "") => {
    const path = `https://api.giphy.com/v1/gifs/search?api_key=${API_KEY}&q=${searchTerm}&limit=${SEARCH_LIMIT}`;
    const matched = [];

    return await fetch(path)
        .then(res => res.json())
        .then(json => {
            return ({
                gifInfo: json.data.map(obj => [obj.images.fixed_width.url, obj.id]),
                showing: json.pagination.count,
                total: json.pagination.total_count,
            });
        })
        .catch(e => e)
};

/**
 * async fetch GET request to get trending gifs
 * @returns Promise with obj with trending gifs
 */
export const trendingGifs = async() => {
    const path = `http://api.giphy.com/v1/gifs/trending?api_key=${API_KEY}&limit=${TRENDING_LIMIT}`;

    return await fetch(path)
        .then((res) => res.json())
        .then(json => {
            return ({
                gifInfo: json.data.map(obj => [obj.images.fixed_width.url, obj.id]),
                showing: json.pagination.count,
                total: json.pagination.total_count,
            });
        })
        .catch(e => console.log(e))
};

/**
 * async fetch GET request to get gif details from its id
 * @param {string} id 
 * @returns Promise gif details: image and id
 */
export const getGifById = async(id) => {
    const path = `http://api.giphy.com/v1/gifs/${id}?api_key=${API_KEY}`;
    //console.log(id);
    return await fetch(path)
        .then((res) => res.json())
        .then(json => {
            return { gifInfo: [json.data.images.fixed_width.url, json.data.id] }
        })
        .catch(e => console.log(e))
};

/**
 * async fetch GET request to get gif details from its id
 * @param {string} id 
 * @returns Promise gif details: id, title, source, bitly_url
 */
export const getGifByIdDetails = async(id) => {

    const path = `http://api.giphy.com/v1/gifs/${id}?api_key=${API_KEY}`;

    return await fetch(path)
        .then((res) => res.json())
        .then(json => {
            return {
                gifDetails: [json.data.id, json.data.title, json.data.username, json.data.bitly_url, json.data.source]
            }
        })
        .catch(e => console.log(e))
};

/**
 * async fetch GET request to get details of multiple
 *  comma-separated gif id
 * @param {[string]} arr of ids
 * @returns Promise array of details of gifs
 */
export const getGifsById = async(arr) => {
    const path = `http://api.giphy.com/v1/gifs?api_key=${API_KEY}&ids=${arr.join(",")}`;
    return await fetch(path)
        .then((res) => res.json())
        .then(json => {
            return ({
                gifInfo: json.data.map(obj => [obj.images.fixed_width.url, obj.id])
            })
        })
        .catch(e => console.log(e))
};


/**
 * async fetch GET request to get details of a random gif
 * @returns Promise random gif details obj: image and id
 */
export const randomGif = async() => {
    const path = `http://api.giphy.com/v1/gifs/random?api_key=${API_KEY}&rating=g`;

    return await fetch(path)
        .then((res) => res.json())
        .then(json => [json.data.images.fixed_width.url, json.data.id])
        .catch(e => console.log(e))
};