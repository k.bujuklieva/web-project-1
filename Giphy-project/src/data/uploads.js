// create an uploads array in localStorage if one doesn't exist
let uploads = JSON.parse(localStorage.getItem('uploads')) || [];

/**
 * adds uploaded gif id to the array uploads in localStorage
 * @param {string} gifId 
 */
export const addUpload = (gifId) => {
    // need to add the success from the POST and the id that will be returned;
    uploads.push(gifId);
    localStorage.setItem('uploads', JSON.stringify(uploads));
};

/**
 * copy of uploads arr with gif ids
 * @returns [string] a copy of uploaded gif ids
 */
export const getUploads = () => [...uploads];