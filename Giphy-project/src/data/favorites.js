// create a favorites array in localStorage if one doesn't exist
let favorites = JSON.parse(localStorage.getItem('favorites')) || [];

/**
 * adds gif id to favorites in localStorage
 * @param {string} gifId 
 * @returns if gif is already in favorites - do nothing
 */
export const addFavorite = (gifId) => {
    if (favorites.find(id => id === gifId)) {
        return;
    }
    favorites.push(gifId);
    localStorage.setItem('favorites', JSON.stringify(favorites));
};

/**
 * removes gif id from favorites in localStorage
 * @param {string} gifId 
 */
export const removeFavorite = (gifId) => {
    favorites = favorites.filter(id => id !== gifId);
    localStorage.setItem('favorites', JSON.stringify(favorites));
};

/**
 * 
 * @returns a copy of the favorites array of gif ids
 */
export const getFavorites = () => [...favorites];