import { searchGifs, trendingGifs, getGifById, randomGif, getGifsById, getGifByIdDetails } from '../data/gifs.js';
import { getFavorites } from '../data/favorites.js';
import { addUpload, getUploads } from '../data/uploads.js';
import { processForm } from '../events/upload-events.js';
import { q } from '../events/helpers.js';

/**
 * async loads gifs from search API
 * @param {string} searchTerm 
 * @returns matchedGifs - results from the search
 */
export const loadSearchGifs = async(searchTerm = '') => {
    const matchedGifs = await searchGifs(searchTerm);

    return matchedGifs;
};

/**
 * async loads gifs from trending API
 * @returns trending gifs
 */
export const loadTrendingGifs = async() => {
    const gifs = await trendingGifs();

    return gifs;
};

/**
 * async loads gif from getGifById API
 * @param {string} id of the gif
 * @returns gif details
 */
export const loadSingleGif = async(id) => {
    const gif = await getGifById(id);

    return gif;
};

/**
 * async loads gif's details from getGifById API
 * @param {string} id of the gif
 * @returns gif details
 */
export const loadSingleGifDetails = async(id) => {
    const gif = await getGifByIdDetails(id);

    return gif;
};

/**
 * loads favorite gifs from localStorage
 * @returns gifs array with ids
 */
export const loadFavoriteGifs = () => {
    const gifs = getFavorites();

    return gifs;
};

/**
 * async loads 1 random gif from Random API
 * @returns 1 random gif details
 */
export const loadRandomGif = async() => {
    const random = await randomGif();
    return random;
};

/**
 * async loads details of uploaded gifs using their id
 * @returns gif ids of uploaded gifs
 */
export const loadUploadedGifs = async() => {
    const uploaded = getUploads();

    const gifs = await getGifsById(uploaded);

    return gifs;
};

/**
 * async loads upload response from Upload API
 * @param {event} event 
 */
export const loadUploadResponse = async(event) => {
    try {
        const response = await processForm(event);
        if (response.status === 200) {
            q("p#file-validation").style.display = 'none';
            q("p#upload-success").style.display = 'block';
            addUpload(response.gifId);
        }
    } catch (e) {
        if (e.message === 'No file attached!') {
            q("p#file-validation").style.display = 'block';
        }
    }
};